# Setup

1. Add to your Podfile:

   ```
   pod 'GD', :git => 'git@gitlab.com:rogerhu/gd-framework.git'
   ```

1. Add to your `xcconfig`:

   ```

   #include "Pods/Target Support Files/Pods-<app name>/Pods-<app-name>.release.xcconfig"
    
   FIPS_PACKAGE=$(CURRENT_ARCH).sdk
   LDPLUSPLUS=${PODS_ROOT}/GD/FIPS_module/$FIPS_PACKAGE/bin/gd_fipsld
   LD=${PODS_ROOT}/GD/FIPS_module/$FIPS_PACKAGE/bin/gd_fipsld
   ENABLE_BITCODE=NO
   ```

# Understanding the code

Use the `lipo` command to extract out the x86 version of the Good Mach-O binrary:

```bash
lipo -thin x86_64 -output GD_x86 Frameworks/GD.framework/Versions/A/GD
```

The output of this file is an archive file (ar) which contains a bunch of object files.  You can unpack the file using the `ar` tool:


```bash
ar -xv GD_x86
```

You can then use [Hopper](https://www.hopperapp.com) to view the code.    The main file to review is the `libgdios4.a-x86_64-master.o`. 

