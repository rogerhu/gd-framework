/*
 * (c) 2017 BlackBerry Limited. All rights reserved.
 */

#ifndef BBDPublicConstans_h
#define BBDPublicConstans_h

#if __has_extension(attribute_deprecated_with_message)
#   define DEPRECATE_BBDTESTOPTION_BBDTESTOPTIONTOUCHID __attribute__((deprecated("BBDTestOptionTouchID option has been deprecated, use XCUIDevice+BiometricsHelpers instead.")))
#else
#   define DEPRECATE_BBDTESTOPTION_BBDTESTOPTIONTOUCHID __attribute__((deprecated))
#endif

typedef NS_OPTIONS(NSInteger, BBDTestOption){
    BBDTestOptionNone,
    BBDTestOptionDisclaimer,
    BBDTestOptionBiometricsID, // simulator only
    BBDTestOptionTouchID DEPRECATE_BBDTESTOPTION_BBDTESTOPTIONTOUCHID
};

#undef DEPRECATE_BBDTESTOPTION_BBDTESTOPTIONTOUCHID

// Text menu items(Cut, Copy, Select All, Paste)
static NSString* const BBDTextMenuItemCopy = @"Copy";

static NSString* const BBDTextMenuItemPaste = @"Paste";

static NSString* const BBDTextMenuItemSelectAll = @"Select All";

static NSString* const BBDTextMenuItemCut = @"Cut";

// Keyboard buttons identifiers
static NSString* const BBDKeyboardHideButton = @"Hide keyboard";

static NSString* const BBDKeyboardDismissButton = @"Dismiss";

#endif /* BBDPublicConstans_h */
