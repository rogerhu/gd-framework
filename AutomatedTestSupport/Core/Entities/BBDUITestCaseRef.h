/*
 * (c) 2017 BlackBerry Limited. All rights reserved.
 *
 */

@import Foundation;
@import XCTest;

#import "BBDPublicConstans.h"

@interface BBDUITestCaseRef : NSObject

@property (nonatomic, weak) XCUIApplication *application;
@property (nonatomic, weak) XCUIDevice *device;
@property (nonatomic, weak) XCTestCase *testCase;
@property (nonatomic) BBDTestOption options;

/**
 * Creates BBDUITestCaseRef instance using XCTestCase and XCUIApplication.
 * Mostly every action for UI testing requires reference to XCUIApplication class - 
 * so we can access element inside view hierarchy, and optionally XCTestCase reference 
 * for such methods like wait, etc.
 * Use this initializer to instantiate object for helper's methods (if needed).
 *
 * @param testCase
 * instance of XCTestCase object
 *
 * @param application
 * instance of target XCUIApplication object
 *
 * @return BBDUITestCaseRef instance, which is used inside helpers to provide target functionality.
 */
- (instancetype)initWithTestCase:(XCTestCase *)testCase
                  forApplication:(XCUIApplication *)application;

@end
