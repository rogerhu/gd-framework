/*
 * (c) 2017 BlackBerry Limited. All rights reserved.
 *
 */

#import "BBDUITestCaseRef.h"

@implementation BBDUITestCaseRef

- (instancetype)initWithTestCase:(XCTestCase *)testCase forApplication:(XCUIApplication *)application
{
    self = [super init];
    if (self) {
        _testCase = testCase;
        _application = application;
        _options = BBDTestOptionNone;
        _device = [XCUIDevice sharedDevice];
    }
    return self;
}

@end
