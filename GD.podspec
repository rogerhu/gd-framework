Pod::Spec.new do |s|

  s.name         = "GD"
  s.version      = "5.0.0.78.6"
  s.summary      = "The BlackBerry Dynamics Platform provides a range of SDKs and plugins that allow you to develop in the environment of your choice and build native, hybrid, or web apps."
  s.description  = "GD Framework "
  s.homepage     = "https://developers.blackberry.com/us/en/resources/get-started/blackberry-dynamics-getting-started.html?platform=ios#step-2"
  s.license      = { :type => 'BSD' }
  s.author       = "Blackberry"
  s.platform     = :ios, "9.0"
  s.source	 = { :git => "https://gitlab.com/rogerhu/gd-framework" }
  s.vendored_frameworks = "Frameworks/GD.framework"
  s.resources = "Frameworks/GD.framework/Versions/A/Resources/*"
  s.libraries	 = ["c++", "z"]
  s.frameworks	 = 'UIKit', 'Foundation', 'WebKit', 'CoreTelephony', 'MessageUI', 'CoreData', 'MobileCoreServices', 'SystemConfiguration', 'LocalAuthentication'
  s.compiler_flags = "-lz -lstdc++"

  s.subspec 'FIPS' do |fips|
    fips.resources = ['FIPS_module/*']
    fips.xcconfig =  {'ENABLE_BITCODE' => 'NO',
  		      'ARCHS' => ['arm64', 'x86_64'],	
                      'LDPLUSPLUS' => '${SRCROOT}/Pods/GD/FIPS_module/$(CURRENT_ARCH).sdk/bin/gd_fipsld',
                      'LD' => '${SRCROOT}/Pods/GD/FIPS_module/$(CURRENT_ARCH).sdk/bin/gd_fipsld'}
  end
end
