#!/bin/sh
# Copyright (c) BlackBerry Dynamics, 2018. All rights reserved.
# Checks if Xcode 9.0 or higher is installed and selected.

SELECTED_XCODE_DIR=`xcode-select -print-path`

if [ -d "${SELECTED_XCODE_DIR}" ]; then
    # Make sure it really is switched as other installation scripts depend on this.
	XCODE_APP=.app
    XCODE_APP_DIR="n/a"
	if [[ "${SELECTED_XCODE_DIR}" == *"$XCODE_APP"* ]]; then
		XCODE_APP_DIR=`expr "${SELECTED_XCODE_DIR}" :  '\(.*\.app\)'`
	else
    	xcode-select -switch "${XCODE_DEV_DIR}" 
	fi

    #at some point 'version.plist' changed its name to begin with a lowercase 'v'
    #in a case-sensitive volume this makes a big difference
    VERSION_PLIST_CASE_SENSITIVE_NAME='version'
    if [ -f "${XCODE_APP_DIR}"/Contents/Version ]; then
    VERSION_PLIST_CASE_SENSITIVE_NAME='Version'
    fi

    # Now locate xcode
    XCODE_VER=`defaults read "${XCODE_APP_DIR}"/Contents/"${VERSION_PLIST_CASE_SENSITIVE_NAME}" CFBundleShortVersionString`
	XCODE_MAJOR_VER=`expr ${XCODE_VER%%.*}`
	XCODE_MINOR_VER=`expr ${XCODE_VER:2:1}`

	# Only Xcode 9.x.x or higher is supported...
	if (( XCODE_MAJOR_VER == 9 )); then
		if (( XCODE_MINOR_VER == 0 )); then
            echo "Xcode 9.0 is selected: `xcode-select -print-path`."
            exit 1
        elif (( XCODE_MINOR_VER == 1 )); then
            echo "Xcode 9.1 or higher is selected: `xcode-select -print-path`."
            exit 1
		elif (( XCODE_MINOR_VER == 2 )); then
            echo "Xcode 9.2 or higher is selected: `xcode-select -print-path`."
            exit 1
        elif (( XCODE_MINOR_VER == 3 )); then
            echo "Xcode 9.3 or higher is selected: `xcode-select -print-path`."
            exit 1
        elif (( XCODE_MINOR_VER == 4 )); then
            echo "Xcode 9.4 or higher is selected: `xcode-select -print-path`."
            exit 1
        fi
	elif (( XCODE_MAJOR_VER == 10 )); then
        XCODE_MINOR_VER=`expr ${XCODE_VER:3:1}`
        if (( XCODE_MINOR_VER == 0 )); then
		    echo "Xcode 10.0 is selected: `xcode-select -print-path`."
		    exit 1
        elif (( XCODE_MINOR_VER == 1 )); then
            echo "Xcode 10.1 is selected: `xcode-select -print-path`."
            exit 1
        elif (( XCODE_MINOR_VER > 1 )); then
            echo "Xcode 10.1 of higher is selected: `xcode-select -print-path`."
            exit 1
        fi
	fi
fi

echo Install or select Xcode 9.0 or higher.
echo To check which Xcode IDE is selected, run xcode-select -print-path from the command line.
echo If xcode-select is not found, then it is likely that Xcode and iOS SDK has not been installed.
echo For Xcode 9.0 or higher from the AppStore xcode-select can be set as follows:
echo sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer/

exit 0
