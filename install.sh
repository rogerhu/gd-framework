#!/bin/sh

# (c) 2017 BlackBerry Limited. All rights reserved.
# Install the GD SDK to Xcode and iOS SDKs.

# Detect if framework installed in legacy Good Technology location
# to create symbolic link from old to new location of framework
LEGACY_ROOT_INSTALL_LOCATION="/Good Technology"
LEGACY_INSTALL_LOCATION_USED="$(test -d "${LEGACY_ROOT_INSTALL_LOCATION}";echo $?)"

# package maker cannot install to home directory,
# so we use tmp folder to relocate to ~/Library/Application Support/BlackBerry
# we remove GD_TMP_INSTALL_FOLDER after installation completed
GD_TMP_INSTALL_FOLDER="/Library/Application Support/BlackBerry"
GD_DEFAULT_INSTALL_FOLDER="$HOME/Library/Application Support/BlackBerry"

# When invoked from the Installer, the chosen installation volume and 
# directory is passed as the 2nd argument. If no arguments,
# the script is assumed to be being invoked by the user.
if [ "$2" ]; then
  # Invoked from Installer. Remove any prior SDK and extract the new one...
  
  GD_ROOT="$2"

  if [ "$2" == "$GD_TMP_INSTALL_FOLDER" ];
  then
    GD_ROOT="$GD_DEFAULT_INSTALL_FOLDER"
  fi

  GD_DIR="${GD_ROOT}"/Good.platform/iOS
  rm -rf "${GD_DIR}"
  mkdir -p "${GD_DIR}"

  echo Extracting payload to "${GD_DIR}"...
  tar -C "${GD_ROOT}" -xf "${2}"/Good.platform.tar
  echo Payload extracted.
  rm -f "${2}"/Good.platform.tar
  chown -R `whoami` "${GD_ROOT}"
  chmod -R u=rwx "${GD_ROOT}"
  # install extensions, if any
  "${GD_DIR}"/install_extensions.sh "${GD_ROOT}"

  # Uninstall any prior Xcode installation or exit if Xcode is not found
  "${GD_DIR}"/uninstall.sh $*
  if [ $? -ne 0 ]; then
  exit 1
  fi

else
  GD_DIR=`dirname "$(cd "${0%/*}" && echo $PWD/${0##*/})"`

  # unpacking FIPS module
  "${GD_DIR}"/install_extensions.sh "${GD_DIR}" ManualInstallationFlag
  # Uninstall any prior Xcode installation or exit if Xcode is not found
  "${GD_DIR}"/uninstall.sh InvokedFromInstallerFlag
  if [ $? -ne 0 ]; then
  exit 1
  fi
fi


echo Installing...

# Xcode SDK paths
SELECTED_SDK_DIR=`xcode-select -print-path`
SDK_DIR="${SELECTED_SDK_DIR}"/Platforms/iPhone
SDK_DEV_DIR="${SDK_DIR}"OS.platform/Developer/SDKs
SDK_SIM_DIR="${SDK_DIR}"Simulator.platform/Developer/SDKs

XCODE_APP=.app
if [[ "${SELECTED_SDK_DIR}" == *"$XCODE_APP"* ]]; then
	XCODE_APP_DIR=`expr "${SELECTED_SDK_DIR}" :  '\(.*\.app\)'`
else
	XCODE_APP_DIR="${SELECTED_SDK_DIR}"/Applications/Xcode.app
fi
if [ -d "${XCODE_APP_DIR}" ]; then
	#at some point 'version.plist' changed its name to begin with a lowercase 'v'
	#in a case-sensitive volume this makes a big difference
	VERSION_PLIST_CASE_SENSITIVE_NAME='version'
	if [ -f "${XCODE_APP_DIR}"/Contents/Version ]; then
		VERSION_PLIST_CASE_SENSITIVE_NAME='Version'
	fi
    XCODE_VER=`defaults read "${XCODE_APP_DIR}"/Contents/"${VERSION_PLIST_CASE_SENSITIVE_NAME}" CFBundleShortVersionString`
    echo Detected Xcode version ${XCODE_VER}
    SDK_TEMPLATE_DIR=~/Library/Developer/Xcode/Templates/iOS/Application
    if [ ! -d "${SDK_TEMPLATE_DIR}" ]; then
        mkdir -p "${SDK_TEMPLATE_DIR}";
    fi
    if [ `expr ${XCODE_VER%%.*}` -eq 8 ]; then
        echo Installing Xcode templates for 8.x....
        GD_TEMPLATE_DIR="${GD_DIR}"/Templates/8.x
        cp -R "${GD_TEMPLATE_DIR}"/* "${SDK_TEMPLATE_DIR}"/
    elif [ `expr ${XCODE_VER%%.*}` -eq 9 ]; then
        echo Installing Xcode templates for 9.x....
        GD_TEMPLATE_DIR="${GD_DIR}"/Templates/9.x
        cp -R "${GD_TEMPLATE_DIR}"/* "${SDK_TEMPLATE_DIR}"/
    elif [ `expr ${XCODE_VER%%.*}` -eq 10 ]; then
        echo Installing Xcode templates for 10.x....
        GD_TEMPLATE_DIR="${GD_DIR}"/Templates/10.x
        cp -R "${GD_TEMPLATE_DIR}"/* "${SDK_TEMPLATE_DIR}"/
    else
        echo GD templates are only supported on Xcode 8.x, 9.x or 10.x versions. Skipping template installation.
    fi
fi

shopt -s expand_aliases

#echo Installing API Reference documentation...
GD_DOCSET="${GD_DIR}"/Documents/com.good.gd.docset
SYSTEM_DOCSET=~/Library/Developer/Shared/Documentation/DocSets/com.good.gd.ios.docset

#create dir if non-existent - fresh xcode install
if [ ! -d "${SYSTEM_DOCSET}" ]; then
    mkdir -p "${SYSTEM_DOCSET}";
fi

cp -R "${GD_DOCSET}"/* "${SYSTEM_DOCSET}"/

# Documentation cannot be viewed in Xcode (See Apple Developer bug 8900323).
# In the interim, refer to the API Reference at https://developer.good.com.
#cp -R "${GD_DOCSET_DIR}"/* "${SDK_DOCSET_DIR}"/

echo Installing static frameworks...

#create symbolic links at legacy location for compatibility
if [ "${LEGACY_INSTALL_LOCATION_USED}" -eq 0 ]; then
	if [ ! -e "${LEGACY_ROOT_INSTALL_LOCATION}" ]; then
		LEGACY_COMPATIBILITY_DIR="${LEGACY_ROOT_INSTALL_LOCATION}/Good.platform"
		mkdir -p "${LEGACY_COMPATIBILITY_DIR}/Frameworks"
		ln -s "${GD_DIR}/FIPS_module" "${LEGACY_COMPATIBILITY_DIR}/FIPS_module"
		ln -s "${GD_DIR}/Frameworks/GD.framework" "${LEGACY_COMPATIBILITY_DIR}/Frameworks/GD.framework"
	fi
fi

GD_FRAMEWRK_DIR="${GD_DIR}"/Frameworks
SDK_FRAMEWRK_DIR=System/Library/Frameworks
SDK_SYMLINK_DIR=/Library/Frameworks

#create dir if non-existent - fresh xcode install
if [ ! -d "${SDK_SYMLINK_DIR}" ]; then
mkdir -p "${SDK_SYMLINK_DIR}";
fi

for sub_dir in $(ls "${SDK_DEV_DIR}")
do
  rsync -K -a "${GD_FRAMEWRK_DIR}"/* "${SDK_DEV_DIR}"/"${sub_dir}"/"${SDK_FRAMEWRK_DIR}"/
done
for sub_dir in $(ls "${SDK_SIM_DIR}")
do
  rsync -K -a "${GD_FRAMEWRK_DIR}"/* "${SDK_SIM_DIR}"/"${sub_dir}"/"${SDK_FRAMEWRK_DIR}"/
done

#list all *.framework inside BB directory
for frmwrks in $(ls "${GD_FRAMEWRK_DIR}"| grep '\.framework$' )
do
#create new link for framework
ln -s "${GD_FRAMEWRK_DIR}"/"$frmwrks" "${SDK_SYMLINK_DIR}"/"$frmwrks"
done

echo Installation complete.
