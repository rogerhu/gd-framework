#!/bin/sh

# Copyright (c) Good Technology, 2011. All rights reserved.
# Remove artifacts from v0.5.1743 and older, and unintsall the GD SDK from Xcode.

# When invoked from the Installer, the chosen installation volume and 
# directory is passed as the 2nd argument. If no arguments,
# the script is assumed to be being invoked by the user.
if [ "$2" ]; then
  # Invoked from Installer.
  GD_DIR=“${PWD}“
else
  GD_DIR=`dirname "$(cd "${0%/*}" && echo $PWD/${0##*/})"`
fi

# Exit if Xcode is not found
"${GD_DIR}"/check_for_supported_dependencies.sh $*
if [ $? -eq 0 ]; then
    exit 1
fi

if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

echo Cleaning...

# Xcode SDK paths
SELECTED_SDK_DIR=`xcode-select -print-path`
SDK_DIR="${SELECTED_SDK_DIR}"/Platforms/iPhone
SDK_DEV_DIR="${SDK_DIR}"OS.platform/Developer/SDKs
SDK_SIM_DIR="${SDK_DIR}"Simulator.platform/Developer/SDKs

# Remove artifacts from v0.5.1743 and older
rm -rf ~/Library/Frameworks/GD.framework
rm -rf ~/Library/Developer/Xcode/Templates/Project\ Templates/Application/Window-based\ Good\ Dynamics\ Application.xctemplate
# Remove artifacts from v2.x.x and older
rm -rf /Good\ Technology


# Uninstall the Xcode project templates...
GD_TEMPLATE_FILE=Single\ View\ Good\ Dynamics\ Application.xctemplate
BD_TEMPLATE_FILE=BlackBerry\ Dynamics.xctemplate
GD_TEMPLATE_ASSETS_FILE=GDAssets.bundle

# Path to template project in Xcode6 has extra folder "iOS"
XCODE_APP=.app
if [[ "${SELECTED_SDK_DIR}" == *"$XCODE_APP"* ]]; then
	XCODE_APP_DIR=`expr "${SELECTED_SDK_DIR}" :  '\(.*\.app\)'`
else
	XCODE_APP_DIR="${SELECTED_SDK_DIR}"/Applications/Xcode.app
fi
if [ -d "${XCODE_APP_DIR}" ]; then
	#at some point 'version.plist' changed its name to begin with a lowercase 'v'
	#in a case-sensitive volume this makes a big difference
	VERSION_PLIST_CASE_SENSITIVE_NAME='version'
	if [ -f "${XCODE_APP_DIR}"/Contents/Version ]; then
		VERSION_PLIST_CASE_SENSITIVE_NAME='Version'
	fi
    
  XCODE_VER=`defaults read "${XCODE_APP_DIR}"/Contents/"${VERSION_PLIST_CASE_SENSITIVE_NAME}" CFBundleShortVersionString`
  echo Detected Xcode version ${XCODE_VER}
  if [ `expr ${XCODE_VER%%.*}` > 7 ] ; then
    echo Uninstalling GD template from Xcode....
    SDK_TEMPLATE_DIR="${SDK_DIR}"OS.platform/Developer/Library/Xcode/Templates/Project\ Templates/iOS/Application
    # uninstall template for 2.x.x
    rm -rf "${SDK_TEMPLATE_DIR}"/"${GD_TEMPLATE_FILE}"
    rm -rf "${SDK_TEMPLATE_DIR}"/"${GD_TEMPLATE_ASSETS_FILE}"
    # uninstall template for 3.x.x
    SDK_TEMPLATE_DIR=~/Library/Developer/Xcode/Templates/iOS/Application
    # uninstall Good Dynamics template
    rm -rf "${SDK_TEMPLATE_DIR}"/"${GD_TEMPLATE_FILE}"
    rm -rf "${SDK_TEMPLATE_DIR}"/"${GD_TEMPLATE_ASSETS_FILE}"
    # uninstall BlackBerry Dynamics template
    rm -rf "${SDK_TEMPLATE_DIR}"/"${BD_TEMPLATE_FILE}"

  else
    echo GD templates are only supported on Xcode 7.x and 8.x versions. Skipping template uninstallation.
  fi
fi

# Uninstall the API Reference documentation from v2.x and older...
GD_DOCSET_FILE=com.good.gd.docset
SDK_DOCSET_DIR="${SDK_DIR}"OS.platform/Developer/Documentation/Docsets
rm -rf "${SDK_DOCSET_DIR}"/"${GD_DOCSET_FILE}"
SDK_SYSTEM_DOCSET_DIR=~/Library/Developer/Shared/Documentation/DocSets
rm -rf "${SDK_SYSTEM_DOCSET_DIR}"/"${GD_DOCSET_FILE}"

# Uninstall the API Reference documentation...
GD_IOS_DOCSET_FILE=com.good.gd.ios.docset
SDK_IOS_DOCSET_DIR="${SDK_DIR}"OS.platform/Developer/Documentation/Docsets
rm -rf "${SDK_IOS_DOCSET_DIR}"/"${GD_IOS_DOCSET_FILE}"
SDK_SYSTEM_IOS_DOCSET_DIR=~/Library/Developer/Shared/Documentation/DocSets
rm -rf "${SDK_SYSTEM_IOS_DOCSET_DIR}"/"${GD_IOS_DOCSET_FILE}"

# Uninstall GD.framework to from each iOS SDK...
GD_FRAMEWRK_FILE=GD.framework
GD_FRAMEWRK_DIR="${GD_DIR}"/Frameworks
SDK_FRAMEWRK_DIR=System/Library/Frameworks
SDK_SYMLINK_DIR=/Library/Frameworks

rm -rf "${SDK_SYMLINK_DIR}"/"${GD_FRAMEWRK_FILE}"

for sub_dir in $(ls "${SDK_DEV_DIR}")
do

  rm -rf "${SDK_DEV_DIR}"/"${sub_dir}"/"${SDK_FRAMEWRK_DIR}"/"${GD_FRAMEWRK_FILE}"
done
for sub_dir in $(ls "${SDK_SIM_DIR}")
do
  rm -rf "${SDK_SIM_DIR}"/"${sub_dir}"/"${SDK_FRAMEWRK_DIR}"/"${GD_FRAMEWRK_FILE}"
done
# $1 means InvokedFromInstallerFlag - we invoke unistaller from the installer,
# so don't remove GD_DIR (in a case of installer $2 exists,
# the following check will return false)

if [ ! "$2" ] && [ ! "$1" ]; then
    # Invoked manually.
    PARENT_DIR="$(dirname "${GD_DIR}")"
    GD_DIR_NAME=$(basename "${PARENT_DIR}")
    CONTENT_COUNT="$(find "${PARENT_DIR}/" -maxdepth 1 ! -iname .DS_Store ! -iname . ! -iname .. ! -iname "${GD_DIR_NAME}" | wc -l)"
    cd "${GD_DIR}"/../../../
    if [ "${CONTENT_COUNT}" -gt "1" ]; then
    # Remove iOS dir only if Good.platform contains other artefacts
    rm -rf BlackBerry/Good.platform/iOS
    else
    # Remove ~/Library/Application Support/BlackBerry
    rm -rf BlackBerry/
    fi
fi

echo Clean complete.
