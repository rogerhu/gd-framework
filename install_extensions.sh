#!/bin/sh

# Copyright (c) Good Technology, 2013. All rights reserved.
# Install the GD SDK extenstions to Xcode and iOS SDKs.

#this script is generated at build time and may do nothing if given build configuration does not contain any extensions

echo "checking for extensions..."

if [ "$1" ]; then
  # Invoked from Installer. Install FIPS module if it is present

  GD_ROOT="$1"

  if [ -e "${GD_ROOT}/Good.platform/iOS/FIPS_module.tar" ]
  then
    echo "Extracting extensions..."
      
    echo Extracting FIPS module payload to "${GD_ROOT}"...
    mkdir "${GD_ROOT}"/Good.platform/iOS/FIPS_module
    tar -C "${GD_ROOT}"/Good.platform/iOS/FIPS_module -xf "${GD_ROOT}"/Good.platform/iOS/FIPS_module.tar
    echo Payload extracted.
    rm -f "${GD_ROOT}"/Good.platform/iOS/FIPS_module.tar
	chown -Rv `whoami` "${GD_ROOT}"/Good.platform/iOS/FIPS_module
  	chmod -Rvv a=rwx "${GD_ROOT}"/Good.platform/iOS/FIPS_module
  fi
fi

if [ "$2" ]; then
# Invoked while Manual Installation. Unpacking FIPS module if it is present

    GD_ROOT="$1"

    if [ -e "${GD_ROOT}/FIPS_module.tar" ]
    then
        echo "Extracting extensions..."

        echo Extracting FIPS module payload to "${GD_ROOT}"...
        mkdir "${GD_ROOT}"/FIPS_module
        tar -C "${GD_ROOT}"/FIPS_module -xf "${GD_ROOT}"/FIPS_module.tar
        echo Payload extracted.
        rm -f "${GD_ROOT}"/FIPS_module.tar
        chown -Rv `whoami` "${GD_ROOT}"/FIPS_module
        chmod -Rvv a=rwx "${GD_ROOT}"/FIPS_module
    fi
fi