/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 *
 */

#pragma once

/**\file GDPushiOS.h
 * Deprecated header.
 * \deprecated This header is deprecated and will be removed in a future release. Use GDPush.h instead.
 */

#warning "GDPushiOS.h is deprecated and will be removed in next release. Use GDPush.h instead."

#import "GDPush.h"
